<?php

namespace App\Controller;

use App\Entity\Viaje;
use App\Entity\Viajero;
use App\Entity\ViajeroViajes;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/viajeros")
 */
class ViajeroController extends BaseAPIController
{
    /**
     * @Route(
     *     name="api_viajero_list",
     *     path="",
     *     methods={"GET"},
     *     defaults={"_api_collection_operation_name"="get"}
     * )
     */
    public function list()
    {
        $records = $this->em->getRepository(Viajero::class)->findAll();

        return $this->jsonResponse($records, "listado de viajeros");
    }

    /**
     * @Route(
     *     name="api_viajero_new",
     *     path="",
     *     methods={"POST"},
     *     defaults={"_api_collection_operation_name"="post"}
     * )
     */
    public function new(Request $request)
    {
        $badRequest = [];
        try {
            // valida que se haya enviando algún parámetro
            if (empty($request->request->all()) && empty(json_decode($request->getContent(), true))) {
                return $this->jsonResponseBadRequest("Debe enviar los parámetros respectivos");
            }

            // obtener parámetros
            $cedula = $this->getBodyParameter($request, 'cedula');
            $nombre = $this->getBodyParameter($request, 'nombre');
            $fechaNacimiento = $this->getBodyParameter($request, 'fechaNacimiento');
            $telefono = $this->getBodyParameter($request, 'telefono');

            // validar parámetros individules
            if (is_null($cedula)) {
                $badRequest[] = "El parámetro cedula es requerido";
            }
            if (is_null($nombre)) {
                $badRequest[] = "El parámetro nombre es requerido";
            }
            if (is_null($fechaNacimiento)) {
                $badRequest[] = "El parámetro fechaNacimiento es requerido";
            } else {
                // comprobar formato de la fecha
                list($fecha, $msj) = $this->formatoFecha($fechaNacimiento);
                if (count($msj) !== 0) {
                    $badRequest[] = implode(". ", $msj);
                }
            }
            if (is_null($telefono)) {
                $badRequest[] = "El parámetro telefono es requerido";
            }

            // si algún parámetro hace falta, regregar error
            if (count($badRequest)) {
                return $this->jsonResponseBadRequest(implode(". ", $badRequest));
            }

            // comprobar/procesar datos antes de guardarlos
            if ($this->em->getRepository(Viajero::class)->findOneBy(["cedula" => $cedula])) {
                return $this->jsonResponseBadRequest("Esta cédula ya se encuentra registrada");
            }

            //->Doctrine: start database transactions
            $this->em->getConnection()->beginTransaction();

            $viajero = new Viajero();
            $viajero->setCedula($cedula);
            $viajero->setNombre($nombre);
            $viajero->setFechaNacimiento(new \DateTime($fecha));
            $viajero->setTelefono($telefono);

            // validar restricciones de la entidad
            $errors = $this->validator->validate($viajero);
            if (count($errors) > 0) {
                return $this->jsonResponseError($errors);
            }

            $this->em->persist($viajero);
            $this->em->flush();

            //->Doctrine: commit transactions to database
            $this->em->getConnection()->commit();
        } catch (Exception $ex) {
            //->Doctrine: cancel database transactions
            $this->em->getConnection()->rollback();
            return $this->jsonResponseError($ex->getMessage());
        }

        return $this->jsonResponseSuccess($viajero, "El Viajero se registró con exito");
    }

    /**
     * @Route(
     *     name="api_viajero_item",
     *     path="/{id}",
     *     methods={"GET"},
     *     defaults={"_api_item_operation_name"="get"}
     * )
     */
    public function item($id)
    {
        // encontrar viajero
        if (is_null($viajero = $this->em->getRepository(Viajero::class)->findOneById($id))) {
            return $this->jsonResponseNotFound("El viajero no fue encontrado");
        }

        return $this->jsonResponse($viajero, "detalle del viajero");
    }

    /**
     * @Route(
     *     name="api_viajero_update",
     *     path="/{id}",
     *     methods={"PUT"},
     *     defaults={"_api_item_operation_name"="put"}
     * )
     */
    public function update(Request $request, $id)
    {
        $badRequest = [];
        try {
            // encontrar viajero
            if (is_null($viajero = $this->em->getRepository(Viajero::class)->findOneById($id))) {
                return $this->jsonResponseNotFound("El viajero no fue encontrado");
            }

            // valida que se haya enviando algún parámetro
            if (empty($request->request->all()) && empty(json_decode($request->getContent(), true))) {
                return $this->jsonResponseBadRequest("Debe enviar algún parámetros a editar");
            }

            // obtener parámetros
            $cedula = $this->getBodyParameter($request, 'cedula');
            $nombre = $this->getBodyParameter($request, 'nombre');
            $fechaNacimiento = $this->getBodyParameter($request, 'fechaNacimiento');
            $telefono = $this->getBodyParameter($request, 'telefono');
            $modificar = false;

            //->Doctrine: start database transactions
            $this->em->getConnection()->beginTransaction();

            // validar parámetros individules y guardar nuevo valor
            if (isset($cedula) && $viajero->getCedula() != $cedula) {
                $viajero->setCedula($cedula);
                $modificar = true;
            }
            if (isset($nombre) && $viajero->getNombre() != $nombre) {
                $viajero->setNombre($nombre);
                $modificar = true;
            }
            if (isset($fechaNacimiento)) {
                // comprobar formato de la fecha
                list($fecha, $msj) = $this->formatoFecha($fechaNacimiento);
                if (count($msj) === 0) {
                    // comparar que las fechas sean diferentes
                    if ($fecha != $viajero->getFechaNacimiento()->format('Y-m-d')) {
                        $viajero->setFechaNacimiento(new \DateTime($fecha));
                        $modificar = true;
                    }
                } else {
                    $badRequest[] = implode(". ", $msj);
                }
            }
            if (isset($telefono) && $viajero->getTelefono() != $telefono) {
                $viajero->setTelefono($telefono);
                $modificar = true;
            }

            // si algún parámetro hace falta, regregar error
            if (count($badRequest)) {
                return $this->jsonResponseBadRequest(implode(". ", $badRequest));
            }

            // comprobar/procesar datos antes de guardarlos
            if ($via = $this->em->getRepository(Viajero::class)->findOneBy(["cedula" => $cedula])) {
                if ($via->getId() != $id) {
                    return $this->jsonResponseBadRequest("Esta cédula ya se encuentra registrada");
                }
            }

            // validar restricciones de la entidad
            $errors = $this->validator->validate($viajero);
            if (count($errors) > 0) {
                return $this->jsonResponseError($errors);
            }

            if ($modificar) {
                $this->em->persist($viajero);
                $this->em->flush();

                //->Doctrine: commit transactions to database
                $this->em->getConnection()->commit();
            } else {
                return $this->jsonResponse($viajero, "Los datos del Viajero no fueron modificados");
            }
        } catch (Exception $ex) {
            //->Doctrine: cancel database transactions
            $this->em->getConnection()->rollback();
            return $this->jsonResponseError($ex->getMessage());
        }

        return $this->jsonResponseSuccess($viajero, "El Viajero se ha actualizado con exito");
    }

    /**
     * @Route(
     *     name="api_viajero_delete",
     *     path="/{id}",
     *     methods={"DELETE"},
     *     defaults={"_api_item_operation_name"="delete"}
     * )
     */
    public function delete($id)
    {
        try {
            // encontrar viajero
            if (is_null($viajero = $this->em->getRepository(Viajero::class)->findOneById($id))) {
                return $this->jsonResponseNotFound("El viajero no fue encontrado");
            }
            // comprobar si el viaje ya ha sido asignado a un viajero
            if ($this->em->getRepository(ViajeroViajes::class)->findBy(['viajero' => $id])) {
                return $this->jsonResponseBadRequest("Este viajero no puede eliminarse ya que se encuentra asignado a algun viaje");
            }
            // se clona el objeto para retornarlo en la respuesta
            $_viajero = clone $viajero;

            $this->em->remove($viajero);
            $this->em->flush();
        } catch (Exception $ex) {
            return $this->jsonResponseError($ex->getMessage());
        }

        return $this->jsonResponse($_viajero, "El Viajero eliminado con exito");
    }

    /**
     * @Route(
     *     name="api_viajero_asignarviajes",
     *     path="/asignar_viajes",
     *     methods={"POST"},
     *     defaults={"_api_collection_item_name"="post_asignar"}
     * )
     */
    public function asignarViajes(Request $request)
    {
        $badRequest = [];
        try {
            // valida que se haya enviando algún parámetro
            if (empty($request->request->all()) && empty(json_decode($request->getContent(), true))) {
                return $this->jsonResponseBadRequest("Debe enviar los parámetros respectivos");
            }

            // obtener parámetros
            $viajeroId = $this->getBodyParameter($request, 'viajeroId');
            $viajesIds = $this->getBodyParameter($request, 'viajesIds');

            // validar parámetros individules
            if (is_null($viajeroId)) {
                $badRequest[] = "El parámetro viajeroId es requerido";
            }
            if (is_null($viajesIds)) {
                $badRequest[] = "El parámetro viajesIds es requerido";
            } else {
                if (count($viajesIds) == 0) {
                    $badRequest[] = "El parámetro viajesIds debe ser un arreglo de almenos una id";
                }
            }

            // si algún parámetro hace falta, regregar error
            if (count($badRequest)) {
                return $this->jsonResponseBadRequest(implode(". ", $badRequest));
            }

            //->Doctrine: start database transactions
            $this->em->getConnection()->beginTransaction();

            // comprobar/procesar datos antes de guardarlos
            // encontrar viajero
            if (is_null($viajero = $this->em->getRepository(Viajero::class)->findOneById($viajeroId))) {
                return $this->jsonResponseNotFound("El viajero no fue encontrado");
            }

            $viajesEncontrados = [];
            $viajesNoEncontrados = [];
            // encontrar viajes
            foreach ($viajesIds as $viajeID) {
                if ($encontrado = $this->em->getRepository(Viaje::class)->findOneById($viajeID)) {
                    $viajesEncontrados[] = $encontrado;
                } else {
                    $viajesNoEncontrados[] = $viajeID;
                }
            }

            if (count($viajesEncontrados) === 0) {
                return $this->jsonResponseBadRequest("Actualmente no existen viajes disponibles");
            }

            $response = [];
            $response['viajero'] = $viajero;
            $response['viajes'] = [];
            $response['viajesNoDisponibles'] = $viajesNoEncontrados;

            foreach ($viajesEncontrados as $viaje) {
                $viajeroViaje = new ViajeroViajes();
                $viajeroViaje->setViajero($viajero);
                $viajeroViaje->setViaje($viaje);

                $this->em->persist($viajeroViaje);
                $this->em->flush();
                $response['viajes'][] = $viajeroViaje;
            }

            //->Doctrine: commit transactions to database
            $this->em->getConnection()->commit();
        } catch (Exception $ex) {
            //->Doctrine: cancel database transactions
            $this->em->getConnection()->rollback();
            return $this->jsonResponseError($ex->getMessage());
        }

        return $this->jsonResponseSuccess($response, "Los viajes se asignaron con exito");
    }

    private function formatoFecha($fecha)
    {
        $msj = [];
        $fechaValida = explode("/", $fecha);

        if (count($fechaValida) === 3) {
            list($dia, $mes, $anio) = $fechaValida;
            if (strlen($dia) != 2 || strlen($mes) != 2 || strlen($anio) != 4) {
                $msj[] = "El parámetro fechaNacimiento no tiene un formato correcto. Debe ser: (dd/mm/yyyy)";
                if (intval($mes) > 31) {
                    $msj[] = "El valor del día no puede ser mayor a 31";
                }
                if (intval($mes) > 12) {
                    $msj[] = "El valor del mes no puede ser mayor a 12";
                }
            }
        } else {
            $msj[] = "El parámetro fechaNacimiento no tiene un formato correcto. Debe ser: (dd/mm/yyyy)";
        }

        if (count($msj) === 0) {
            if (checkdate($mes, $dia, $anio)) {
                $fecha = $anio . "-" . $mes . "-" . $dia;
            } else {
                $msj[] = "La fecha de nacimiento '$fecha' no es válida";
            }
        }

        return [$fecha, $msj];
    }
}
