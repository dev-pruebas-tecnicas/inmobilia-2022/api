<?php

namespace App\Controller;

use App\Entity\ViajeroViajes;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/viajero_viajes")
 */
class ViajeroViajesController extends BaseAPIController
{
    /**
     * @Route(
     *     name="api_viajeroviaje_list",
     *     path="",
     *     methods={"GET"},
     *     defaults={"_api_collection_operation_name"="get"}
     * )
     */
    public function list()
    {
        $records = $this->em->getRepository(ViajeroViajes::class)->findAll();

        return $this->jsonResponse($records, "listado de viajes de los viajero");
    }

    /**
     * @Route(
     *     name="api_viajeroviaje_item",
     *     path="/{id}",
     *     methods={"GET"},
     *     defaults={"_api_item_operation_name"="get"}
     * )
     */
    public function item($id)
    {
        // encontrar viaje
        if (is_null($viajeroViaje = $this->em->getRepository(ViajeroViajes::class)->findOneById($id))) {
            return $this->jsonResponseNotFound("El viaje del viajero no fue encontrado");
        }

        return $this->jsonResponse($viajeroViaje, "detalle del viaje del viajero");
    }

}
