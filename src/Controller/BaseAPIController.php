<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BaseAPIController extends AbstractController
{
    private const CONTEXT_GROUPS = ['groups' => ['admin', 'read', 'write']];

    protected $em;
    protected $serializer;
    protected $validator;

    public function __construct(
        EntityManagerInterface $em,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->em = $em;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function jsonResponse($data, $msg = "")
    {
        $response = [];
        $response['statusCode'] = 200;
        $response['data'] = $data;
        $response['message'] = $msg;

        return $this->json($response, 200, [], self::CONTEXT_GROUPS);
    }

    public function jsonResponseSuccess($data, $msg = "¡Solicitud Exitosa!")
    {
        $response = [];
        $response['statusCode'] = 201;
        $response['data'] = $data;
        $response['message'] = $msg;

        return $this->json($response, 201, [], self::CONTEXT_GROUPS);
    }

    public function jsonResponseBadRequest($msg)
    {
        $response = [];
        $response['statusCode'] = 400;
        $response['error'] = true;
        $response['message'] = $msg;

        return $this->json($response, 400, [], self::CONTEXT_GROUPS);
    }

    public function jsonResponseNotFound($msg = "¡Recurso No encontrado!")
    {
        $response = [];
        $response['statusCode'] = 404;
        $response['message'] = $msg;

        return $this->json($response, 404, [], self::CONTEXT_GROUPS);
    }

    public function jsonResponseError($msg = "")
    {
        if (empty($msg)) {
            $msg = "¡Error Interno del Servidor!";
        }
        $response = [];
        $response['statusCode'] = 500;
        $response['error'] = true;
        $response['message'] = $msg;

        return $this->json($response, 500, [], self::CONTEXT_GROUPS);
    }

    /**
     * obtener el valor del parámetro enviado en la solicitud.
     * valida si es enviado por form-data o raw
     */
    public function getBodyParameter(Request $request, string $paramName)
    {
        $value = $request->request->get($paramName, null);
        if (is_null($value)) {
            $parameters = json_decode($request->getContent(), true);
            return $parameters[$paramName] ?? null;
        }

        return $value;
    }
}
