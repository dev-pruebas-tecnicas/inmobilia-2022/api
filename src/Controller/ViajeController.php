<?php

namespace App\Controller;

use App\Entity\Viaje;
use App\Entity\ViajeroViajes;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/viajes")
 */
class ViajeController extends BaseAPIController
{
    /**
     * @Route(
     *     name="api_viaje_list",
     *     path="",
     *     methods={"GET"},
     *     defaults={"_api_collection_operation_name"="get"}
     * )
     */
    public function list()
    {
        $records = $this->em->getRepository(Viaje::class)->findAll();

        return $this->jsonResponse($records, "listado de viajes");
    }

    /**
     * @Route(
     *     name="api_viaje_new",
     *     path="",
     *     methods={"POST"},
     *     defaults={"_api_collection_operation_name"="post"}
     * )
     */
    public function new(Request $request)
    {
        $badRequest = [];
        try {
            // valida que se haya enviando algún parámetro
            if (empty($request->request->all()) && empty(json_decode($request->getContent(), true))) {
                return $this->jsonResponseBadRequest("Debe enviar los parámetros respectivos");
            }

            // obtener parámetros
            $codigo = $this->getBodyParameter($request, 'codigo');
            $numeroPlazas = $this->getBodyParameter($request, 'numeroPlazas');
            $origen = $this->getBodyParameter($request, 'origen');
            $destino = $this->getBodyParameter($request, 'destino');
            $precio = $this->getBodyParameter($request, 'precio');

            // validar parámetros individules
            if (is_null($codigo)) {
                $badRequest[] = "El parámetro codigo es requerido";
            }
            if (is_null($numeroPlazas)) {
                $badRequest[] = "El parámetro numeroPlazas es requerido";
            }
            if (is_null($origen)) {
                $badRequest[] = "El parámetro origen es requerido";
            }
            if (is_null($destino)) {
                $badRequest[] = "El parámetro destino es requerido";
            }
            if (is_null($precio)) {
                $badRequest[] = "El parámetro precio es requerido";
            }

            // si algún parámetro hace falta, regregar error
            if (count($badRequest)) {
                return $this->jsonResponseBadRequest(implode(". ", $badRequest));
            }

            // comprobar/procesar datos antes de guardarlos
            if ($this->em->getRepository(Viaje::class)->findOneBy(["codigo" => $codigo])) {
                return $this->jsonResponseBadRequest("El código ya se encuentra registrado");
            }

            //->Doctrine: start database transactions
            $this->em->getConnection()->beginTransaction();

            $viaje = new Viaje();
            $viaje->setCodigo($codigo);
            $viaje->setNumeroPlazas(intval($numeroPlazas));
            $viaje->setOrigen($origen);
            $viaje->setDestino($destino);
            $viaje->setPrecio(floatval($precio));

            // validar restricciones de la entidad
            $errors = $this->validator->validate($viaje);
            if (count($errors) > 0) {
                return $this->jsonResponseError($errors);
            }

            $this->em->persist($viaje);
            $this->em->flush();

            //->Doctrine: commit transactions to database
            $this->em->getConnection()->commit();
        } catch (Exception $ex) {
            //->Doctrine: cancel database transactions
            $this->em->getConnection()->rollback();
            return $this->jsonResponseError($ex->getMessage());
        }

        return $this->jsonResponseSuccess($viaje, "El Viaje se registró con exito");
    }

    /**
     * @Route(
     *     name="api_viaje_item",
     *     path="/{id}",
     *     methods={"GET"},
     *     defaults={"_api_item_operation_name"="get"}
     * )
     */
    public function item($id)
    {
        // encontrar viaje
        if (is_null($viaje = $this->em->getRepository(Viaje::class)->findOneById($id))) {
            return $this->jsonResponseNotFound("El viaje no fue encontrado");
        }

        return $this->jsonResponse($viaje, "detalle del viaje");
    }

    /**
     * @Route(
     *     name="api_viaje_update",
     *     path="/{id}",
     *     methods={"PUT"},
     *     defaults={"_api_item_operation_name"="put"}
     * )
     */
    public function update(Request $request, $id)
    {
        $badRequest = [];
        try {
            // encontrar viaje
            if (is_null($viaje = $this->em->getRepository(Viaje::class)->findOneById($id))) {
                return $this->jsonResponseNotFound("El viaje no fue encontrado");
            }

            // valida que se haya enviando algún parámetro
            if (empty($request->request->all()) && empty(json_decode($request->getContent(), true))) {
                return $this->jsonResponseBadRequest("Debe enviar algún parámetros a editar");
            }

            // obtener parámetros
            $codigo = $this->getBodyParameter($request, 'codigo');
            $numeroPlazas = $this->getBodyParameter($request, 'numeroPlazas');
            $origen = $this->getBodyParameter($request, 'origen');
            $destino = $this->getBodyParameter($request, 'destino');
            $precio = $this->getBodyParameter($request, 'precio');
            $modificar = false;

            //->Doctrine: start database transactions
            $this->em->getConnection()->beginTransaction();

            // validar parámetros individules y guardar nuevo valor
            if (isset($codigo) && $viaje->getCodigo() != $codigo) {
                $viaje->setCodigo($codigo);
                $modificar = true;
            }
            if (isset($numeroPlazas) && $viaje->getNumeroPlazas() != intval($numeroPlazas)) {
                $viaje->setNumeroPlazas($numeroPlazas);
                $modificar = true;
            }
            if (isset($origen) && $viaje->getOrigen() != $origen) {
                $viaje->setOrigen($origen);
                $modificar = true;
            }
            if (isset($destino) && $viaje->getDestino() != $destino) {
                $viaje->setDestino($destino);
                $modificar = true;
            }
            if (isset($precio) && $viaje->getPrecio() != floatval($precio)) {
                $viaje->setPrecio($precio);
                $modificar = true;
            }

            // si algún parámetro hace falta, regregar error
            if (count($badRequest)) {
                return $this->jsonResponseBadRequest(implode(". ", $badRequest));
            }

            // comprobar/procesar datos antes de guardarlos
            if ($via = $this->em->getRepository(Viaje::class)->findOneBy(["codigo" => $codigo])) {
                if ($via->getId() != $id) {
                    return $this->jsonResponseBadRequest("Este código ya se encuentra en uso");
                }
            }

            // validar restricciones de la entidad
            $errors = $this->validator->validate($viaje);
            if (count($errors) > 0) {
                return $this->jsonResponseError($errors);
            }

            if ($modificar) {
                $this->em->persist($viaje);
                $this->em->flush();

                //->Doctrine: commit transactions to database
                $this->em->getConnection()->commit();
            } else {
                return $this->jsonResponse($viaje, "Los datos del Viaje no fueron modificados");
            }
        } catch (Exception $ex) {
            //->Doctrine: cancel database transactions
            $this->em->getConnection()->rollback();
            return $this->jsonResponseError($ex->getMessage());
        }

        return $this->jsonResponseSuccess($viaje, "El Viaje se ha actualizado con exito");
    }

    /**
     * @Route(
     *     name="api_viaje_delete",
     *     path="/{id}",
     *     methods={"DELETE"},
     *     defaults={"_api_item_operation_name"="delete"}
     * )
     */
    public function delete($id)
    {
        try {
            // encontrar viaje
            if (is_null($viaje = $this->em->getRepository(Viaje::class)->findOneById($id))) {
                return $this->jsonResponseNotFound("El viaje no fue encontrado");
            }
            // comprobar si el viaje ya ha sido asignado a un viajero
            if ($this->em->getRepository(ViajeroViajes::class)->findBy(['viaje' => $id])) {
                return $this->jsonResponseBadRequest("Este viaje no puede eliminarse ya que se encuentra asignado a algun viajero");
            }
            // se clona el objeto para retornarlo en la respuesta
            $_viaje = clone $viaje;

            $this->em->remove($viaje);
            $this->em->flush();
        } catch (Exception $ex) {
            return $this->jsonResponseError($ex->getMessage());
        }

        return $this->jsonResponse($_viaje, "El Viaje eliminado con exito");
    }
}
