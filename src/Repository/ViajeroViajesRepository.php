<?php

namespace App\Repository;

use App\Entity\ViajeroViajes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ViajeroViajes>
 *
 * @method ViajeroViajes|null find($id, $lockMode = null, $lockVersion = null)
 * @method ViajeroViajes|null findOneBy(array $criteria, array $orderBy = null)
 * @method ViajeroViajes[]    findAll()
 * @method ViajeroViajes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ViajeroViajesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ViajeroViajes::class);
    }

    public function add(ViajeroViajes $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ViajeroViajes $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return ViajeroViajes[] Returns an array of ViajeroViajes objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('v.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ViajeroViajes
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
