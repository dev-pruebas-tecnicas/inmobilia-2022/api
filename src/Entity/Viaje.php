<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ViajeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 *     collectionOperations={
 *         "get",
 *         "post"
 *     },
 *     itemOperations={
 *         "get",
 *         "put",
 *         "delete"
 *     }
 * )
 * @ORM\Entity(repositoryClass=ViajeRepository::class)
 */
class Viaje
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("read")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255, unique=true)
     * @Groups({"read", "write"})
     */
    private $codigo;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="integer")
     * @Groups({"read", "write"})
     */
    private $numeroPlazas;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     * @Groups({"read", "write"})
     */
    private $origen;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     * @Groups({"read", "write"})
     */
    private $destino;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="float")
     * @Groups({"read", "write"})
     */
    private $precio;

    /**
     * @ORM\OneToMany(targetEntity=ViajeroViajes::class, mappedBy="viaje")
     */
    private $viajeros;

    public function __construct()
    {
        $this->viajeros = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = strtoupper($codigo);

        return $this;
    }

    public function getNumeroPlazas(): ?int
    {
        return $this->numeroPlazas;
    }

    public function setNumeroPlazas(int $numeroPlazas): self
    {
        $this->numeroPlazas = $numeroPlazas;

        return $this;
    }

    public function getOrigen(): ?string
    {
        return $this->origen;
    }

    public function setOrigen(string $origen): self
    {
        $this->origen = $origen;

        return $this;
    }

    public function getDestino(): ?string
    {
        return $this->destino;
    }

    public function setDestino(string $destino): self
    {
        $this->destino = $destino;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * @return Collection<int, ViajeroViajes>
     */
    public function getViajeros(): Collection
    {
        return $this->viajeros;
    }

    public function addViajero(ViajeroViajes $viajeros): self
    {
        if (!$this->viajeros->contains($viajeros)) {
            $this->viajeros[] = $viajeros;
            $viajeros->setViaje($this);
        }

        return $this;
    }

    public function removeViajero(ViajeroViajes $viajeros): self
    {
        if ($this->viajeros->removeElement($viajeros)) {
            // set the owning side to null (unless already changed)
            if ($viajeros->getViajero() === $this) {
                $viajeros->setViaje(null);
            }
        }

        return $this;
    }
}
