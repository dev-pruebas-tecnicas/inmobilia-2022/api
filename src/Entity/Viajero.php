<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ViajeroRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 *     collectionOperations={
 *         "get",
 *         "post",
 *         "post_asignar"={
 *             "method"="POST",
 *             "route_name"="api_viajero_asignarviajes",
 *             "controller"=ViajeroController::class,
 *         },
 *     },
 *     itemOperations={
 *         "get",
 *         "put",
 *         "delete",
 *     }
 * )
 * @ORM\Entity(repositoryClass=ViajeroRepository::class)
 */
class Viajero
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("read")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @Assert\Length(min=6)
     * @ORM\Column(type="string", length=255, unique=true)
     * @Groups({"read", "write"})
     */
    private $cedula;

    /**
     * @Assert\NotBlank
     * @Assert\Length(min=3)
     * @ORM\Column(type="string", length=255)
     * @Groups({"read", "write"})
     */
    private $nombre;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="date")
     * @Groups({"read", "write"})
     */
    private $fechaNacimiento;

    /**
     * @Assert\NotBlank
     * @Assert\Length(min=7)
     * @ORM\Column(type="string", length=255)
     * @Groups({"read", "write"})
     */
    private $telefono;

    /**
     * @ORM\OneToMany(targetEntity=ViajeroViajes::class, mappedBy="viajero")
     */
    private $viajes;

    public function __construct()
    {
        $this->viajes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCedula(): ?string
    {
        return $this->cedula;
    }

    public function setCedula(string $cedula): self
    {
        $this->cedula = $cedula;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @Groups("read")
     */
    public function getFechaNacimientoFormato()
    {
        return $this->getFechaNacimiento()->format("d/m/Y");
    }

    public function getFechaNacimiento(): ?\DateTimeInterface
    {
        return $this->fechaNacimiento;
    }

    public function setFechaNacimiento(\DateTimeInterface $fechaNacimiento): self
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * @Groups("read")
     */
    public function getListadoViajes()
    {
        return $this->getViajes()->toArray();
    }

    /**
     * @return Collection<int, ViajeroViajes>
     */
    public function getViajes(): Collection
    {
        return $this->viajes;
    }

    public function addViaje(ViajeroViajes $viajes): self
    {
        if (!$this->viajes->contains($viajes)) {
            $this->viajes[] = $viajes;
            $viajes->setViajero($this);
        }

        return $this;
    }

    public function removeViaje(ViajeroViajes $viajes): self
    {
        if ($this->viajes->removeElement($viajes)) {
            // set the owning side to null (unless already changed)
            if ($viajes->getViajero() === $this) {
                $viajes->setViajero(null);
            }
        }

        return $this;
    }
}
