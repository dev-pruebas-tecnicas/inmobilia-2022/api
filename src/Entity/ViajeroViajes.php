<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ViajeroViajesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 *     collectionOperations={
 *         "get",
 *     },
 *     itemOperations={
 *         "get",
 *     }
 * )
 * @ORM\Entity(repositoryClass=ViajeroViajesRepository::class)
 */
class ViajeroViajes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("read")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Viajero::class, inversedBy="viajes")
     * @Groups("read")
     */
    private $viajero;

    /**
     * @ORM\ManyToOne(targetEntity=Viaje::class, inversedBy="viajeros")
     * @Groups("read")
     */
    private $viaje;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getViajero(): ?Viajero
    {
        return $this->viajero;
    }

    public function setViajero(?Viajero $viajero): self
    {
        $this->viajero = $viajero;

        return $this;
    }

    public function getViaje(): ?Viaje
    {
        return $this->viaje;
    }

    public function setViaje(?Viaje $viaje): self
    {
        $this->viaje = $viaje;

        return $this;
    }
}
